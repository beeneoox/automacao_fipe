# automacao_fipe



###### PROJETO FIPE - AUTOMAÇÃO ######

1. Realizar checkout do código fonte;
2. Importar projeto no Eclipse (Maven Project);
3. Aguardar o Maven carregar todas as dependências;
4. Abrir o arquivo .feature (src/test/java/features/ValidaApiFipe.feature);
5. No arquivo aberto, clicar com o botão direito < Run As < Cucumber Feature.

Obs.: Caso a opção 'Cucumber Feature' não seja apresentada, verificar se o plugin do Cucumber está instalado no Eclipse.

###### INSTALAÇÃO PLUGIN CUCUMBER ######
https://www.toolsqa.com/cucumber/install-cucumber-eclipse-plugin/